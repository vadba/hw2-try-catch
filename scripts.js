const list = document.querySelector('#root');
const ulContent = document.createElement('ul');
ulContent.classList.add('ul-content');

const books = [
    {
        author: 'Люсі Фолі',
        name: 'Список запрошених',
        price: 70,
    },
    {
        author: 'Сюзанна Кларк',
        name: 'Джонатан Стрейндж і м-р Норрелл',
    },
    {
        name: 'Дизайн. Книга для недизайнерів.',
        price: 70,
    },
    {
        author: 'Алан Мур',
        name: 'Неономікон',
        price: 70,
    },
    {
        author: 'Террі Пратчетт',
        name: 'Рухомі картинки',
        price: 40,
    },
    {
        author: 'Анґус Гайленд',
        name: 'Коти в мистецтві',
    },
];

const countLenghtObj = books.map(item => Object.keys(item).length);

const hiteLenght = Math.max(...countLenghtObj);
const etalonObj = books.find(obj => Object.keys(obj).length === hiteLenght);

const propeties = Object.getOwnPropertyNames(etalonObj);

books.map(item => {
    try {
        if (!!item && Object.keys(item).length === hiteLenght) {
            const listLi = document.createElement('li');
            listLi.classList.add('list');
            const ulItem = document.createElement('ul');
            const listItemTitle = document.createElement('li');
            listItemTitle.textContent = 'BOOK';
            listItemTitle.classList.add('list-item-title');
            ulItem.append(listItemTitle);
            for (const key in item) {
                if (Object.hasOwnProperty.call(item, key)) {
                    const listItem = document.createElement('li');
                    listItem.classList.add('list-item');
                    listItem.textContent = key + ' - ' + item[key];
                    ulItem.append(listItem);
                }
            }
            listLi.append(ulItem);
            ulContent.append(listLi);
        } else {
            let error = '';
            propeties.map(key => {
                if (!item.hasOwnProperty(key)) {
                    error = key;
                }
            });

            throw new Error(`Відсутня властивість ${error}`);
        }
    } catch (e) {
        console.error('Помилка в данних ->', e.message);
    }
});

list.append(ulContent);
